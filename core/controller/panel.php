<?php

/*
Plugin Name: ShortCodes
Description: Custom shortcodes
Version: v1.0
Author: Ovidiu S Bokar
Author URI: http://www.bovidiu.com
*/
require_once(dirname(__FILE__).'/../library/style.php');


class PanelBoard
{
	
	public function initPanelBoard($sets,$content){
		$align="full_width";
		$bgColor = "grey";

		if(!empty($sets)){
			$align=$sets["a"];
			$bgColor = $sets["c"];
		}
		
		return '<div class="panel" style="'.$this->setAlign($align).$this->setBgColor($bgColor).'">'.strip_tags($content, '<strong><em><br><b><ul><li><ol><a>').'</div>';
	
	}

	private function setBgColor($arg){
		$styles = new GeneralStyles();
		$globalStyles = $styles->initGlobal();
		$colorList = $globalStyles["panel"]["color_list"];
		if($this->checkBgColor($arg)){
			return $colorList[$arg];
		}else{
			return $colorList["grey"];
		}

	}
	private function setAlign($arg){
		$styles = new GeneralStyles();
		$globalStyles = $styles->initGlobal();
		$colorList = $globalStyles["panel"];
		if($this->checkBgColor($arg)){
			return $colorList[$arg];
		}else{
			return $colorList["full_width"];
		}

	}

	private function checkBgColor($set){
		if(!is_null($set) || !empty($set)){
			return true;
		}
		return false;
	}

	private function checkAlignment($set){
		if(!is_null($set) || !empty($set)){
			return true;
		}
		return false;
	}

}