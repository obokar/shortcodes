<?php
/*
Plugin Name: ShortCodes
Description: Custom shortcodes
Version: v1.0
Author: Ovidiu S Bokar
Author URI: http://www.bovidiu.com
*/

require_once(dirname(__FILE__).'/../library/style.php');

class Quote{
    
    private $counter = 0;

    public function initGrabber($content){
        $arg = array();
        if($this->checkForEmptyString($content)){
            $con = explode("\n", $content);
        
            for($i = 1; $i <=count($con); $i++){
                $arg[] = $this->getSinglevalue($con[$i]);
            }

            
        }
        return $this->setWrapper($arg);

    }

    private function getSinglevalue($argStr){
        $arg = array();
        $argStr = strip_tags($argStr);
        if($this->checkForEmptyString($argStr)){
            $this->addCount();
            if(strstr($argStr,"[green]")){
                $cleanGreen = $this->cleanTags('green',$argStr);
                $arg[] = $this->setGreen($cleanGreen);
            }
            if(strstr($argStr,"[blue]")){
                $cleanBlue = $this->cleanTags('blue',$argStr);
                $arg[] = $this->setBlue($cleanBlue);
            }
        }
        return implode("",$arg);
    }

    private function cleanTags($tags,$content){
        $tag = str_replace("[".$tags."]", "", $content);
        return str_replace("[/".$tags."]", "", $tag);
    }
    private function setGreen($argString){
        $style=new GeneralStyles();
        $in = $style->initGlobal( $this->getCountParity() );
        return '<div style="'.$in['quote']['green'].'"><div style="'.$in['quote']['green_arrow'].'"></div><div class="blockquote-inner">'.$argString.'</div></div>';
    }

    private function setBlue($argString){
        $style=new GeneralStyles();
        $in = $style->initGlobal( $this->getCountParity() );
        return '<div style="'.$in['quote']['blue'].'"><div style="'.$in['quote']['blue_arrow'].'"></div><div class="blockquote-inner">'.$argString.'</div></div>';
    }

    private function setWrapper($argString){
        $style=new GeneralStyles();
        $in = $style->initGlobal();
        return '<div class="blockquote-wrapper" style="'.$in['quote']['wrapper'].'">'.implode("",$argString).'</div>';
    }

    private function checkForEmptyString($arg){
        if(empty($arg) || is_null($arg)){
            return false;
        }
        return true;
    }
    
    private function addCount(){
        $this->counter++;
    }
    
    private function getCountParity(){
        if ( $this->counter % 2 == 0 ){
            return 1;
        } else return 0;
    }
    
}