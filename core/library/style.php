<?php

/*
Plugin Name: ShortCodes
Description: Custom shortcodes
Version: v1.0
Author: Ovidiu S Bokar
Author URI: http://www.bovidiu.com
*/

class GeneralStyles{

    private function convertToString($argArray){
        if(!is_array($argArray)){
            return 'Values needs to be in an array($key = $value) ';exit;
        }
        $opt = array();
        foreach ($argArray as $key => $value) {
            $opt[] = $key.':'.$value.'; '; 
        }
        return implode('',$opt); 

    }
    private function getQuoteWrapper(){
        $style = array(
            'float'         => 'right',
            'width'         => '50%',
            'padding'       => '10px 0px 10px 20px',
            'font-family'   => '\'Proxima Italic\', Arial, Helvetica, sans-serif !important',
            'font-size'     => '20px',
            'line-height'   => '1em',
            'position'      => 'relative'
        );
        return $this->convertToString($style);
    }

    private function getQuoteArticleBlue(){
        $style = array(
            'background-color'      => '#0098aa',
            'color'                 => 'white',
            'padding'               => '10px 13px',
            'margin-bottom'         => '20px',
            '-webkit-border-radius' => '5px',
            '-moz-border-radius'    => '5px',
            'border-radius'         => '5px',
            'position'              => 'relative'
        );
        return $this->convertToString($style);
    }
    private function getQuoteArticleGreen(){
        $style = array(
            'background-color'      => '#8fd400',
            'color'                 => 'white',
            'padding'               => '10px 13px',
            'margin-left'           => '50px',
            '-webkit-border-radius' => '5px',
            '-moz-border-radius'    => '5px',
            'border-radius'         => '5px',
            'position'              => 'relative'
        );
        return $this->convertToString($style);
    }

    private function getQuoteGreenArrow(){
        $style = array(
            'width'         => '0', 
            'height'        => '0', 
            'border-left'   => '10px solid transparent',
            'border-right'  => '10px solid transparent',
            'border-bottom' => '10px solid #8fd400',
            'position'      => 'absolute',
            'bottom'        => '42%',
            'right'         => '38%'
        );
        return $this->convertToString($style);
    }

    private function getQuoteBlueArrow(){
        $style = array(
            'width'         => '0', 
            'height'        => '0', 
            'border-left'   => '10px solid transparent',
            'border-right'  => '10px solid transparent',
            'border-top'    => '10px solid #0098aa',
            'position'      => 'absolute',
            'bottom'        => '50%',
            'left'          => '50%'
        );
        return $this->convertToString($style);
    }

    private function getQuoteArrow( $parity , $colour ){
        
        $style = array(
            'width'         => '0', 
            'height'        => '0', 
            'border-left'   => '10px solid transparent',
            'border-right'  => '10px solid transparent',
            'position'      => 'absolute',
            'left'          => '50%'
        );
        
        if( $parity ){
            $style['border-bottom'] = '10px solid ' . $colour;
            $style['top'] = '0';
            $style['margin-top'] = '-10px';
        } else {
            $style['border-top'] = '10px solid ' . $colour;
            $style['bottom'] = '0';
            $style['margin-bottom'] = '-10px';
        }
        
        return $this->convertToString($style);
    }   

    private function getQuoteQuoteOpen(){
        $style = array(
            'content'       => 'open-quote',
            'font-size'     => '30px',
            'left'          => '7px',
            'margin-top'    => '2px',
            'position'      => 'absolute'
        );
        return $this->convertToString($style);
    }

    private function getQuoteQuoteClose(){
        $style = array(
            'content'       => 'close-quote',
            'font-size'     => '30px',
            'margin-top'    => '2px',
            'position'      => 'absolute'
        );
        return $this->convertToString($style);
    }

    private function getPanelLeftAlign(){
        $style = array(
            'width'     => '40%',
            'float'     => 'left',
            'margin'    => '5px 20px 15px 0px',

        );
        return $this->convertToString($style);
    }

    private function getPanelRightAlign(){
        $style = array(
            'width'     => '40%',
            'float'     => 'right',
            'margin'    => '5px 0px 15px 20px',
        );
        return $this->convertToString($style);
    }

    private function getPanelFullWidth(){
        $style = array(
            'width'     => '100%',
            'clear'     => 'both',
            'margin'    => '5px 0px 15px 0px',
        );
        return $this->convertToString($style);
    }

    private function getPanelBackgroundColorList(){
        $color = array(
            'blue'      => $this->convertToString(array(
                            'border'                => 'none !important',
                            'color'                 => '#ffffff',
                            'text-align'            => 'left',
                            'line-height'           => '1.2em',
                            'background'            => '#0098aa',
                            'border-color'          => '#ddd',
                            'padding'               => '20px',
                            'border'                => '1px solid transparent',
                            'border-radius'         => '4px',
                        )),
            'light_blue' => $this->convertToString(array(
                            'border'                => 'none !important',
                            'text-align'            => 'left',
                            'line-height'           => '1.2em',
                            'padding'               => '20px',
                            'background'            => '#cceaee',
                            'color'                 => '#000000 !important',
                            'border-color'          => '#ddd',
                            'border'                => '1px solid transparent',
                            'border-radius'         => '4px',

                        )),
            'orange'    => $this->convertToString(array(
                            'border'        => 'none !important',
                            'color'         => '#ffffff',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#ff8043',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',

                        )),
            'light_orange'  => $this->convertToString(array(
                            'border'        => 'none !important',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#ffe6d9',
                            'color'         => '#000000 !important',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',
                        )),
            'green' => $this->convertToString(array(
                            'border'        => 'none !important',
                            'color'         => '#ffffff',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#8fd400',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',

                        )),
            'light_green'   => $this->convertToString(array(
                            'border'        => 'none !important',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#e9f6cc',
                            'color'         => '#000000 !important',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',
                        )),
            'pink'  => $this->convertToString(array(
                            'border'        => 'none !important',
                            'color'         => '#ffffff',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#e81e75',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',

                        )),
            'light_pink'    => $this->convertToString(array(
                            'border'        => 'none !important',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#fad2e3',
                            'color'         => '#000000 !important',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',
                        )),
            'grey'  => $this->convertToString(array(
                            'border'        => 'none !important',
                            'color'         => '#ffffff',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#a7a9ac',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',                          
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',

                        )),
            'light_grey'    => $this->convertToString(array(
                            'border'        => 'none !important',
                            'text-align'    => 'left',
                            'line-height'   => '1.2em',
                            'background'    => '#cccccc',
                            'color'         => '#000000 !important',
                            'border-color'  => '#ddd',
                            'padding'       => '20px',
                            'border'        => '1px solid transparent',
                            'border-radius' => '4px',
                        ))
        );
        return $color;
    }
    

//INIT METHOD
    public function initGlobal( $parity = 0 ){
        $settings = array(
            'quote' => array(
                'wrapper'       => $this->getQuoteWrapper(),
                'blue'          => $this->getQuoteArticleBlue(),
                'green'         => $this->getQuoteArticleGreen(),
                'green_arrow'   => $this->getQuoteArrow( $parity , '#8fd400'),
                'blue_arrow'    => $this->getQuoteArrow( $parity , '#0098aa' ),
                'quote_open'    => $this->getQuoteQuoteOpen(),
                'quote_close'   => $this->getQuoteQuoteClose()
                
            ),
            'panel' => array(
                'left'              => $this->getPanelLeftAlign(),
                'right'             => $this->getPanelRightAlign(),
                'full_width'        => $this->getPanelFullWidth(),
                'color_list'        => $this->getPanelBackgroundColorList()
            )
        );
        return $settings;
    }


}