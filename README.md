# How to use #

## Blockquotes ##

Use the shortcode "[quote]" with the closing tag "[/quote]".
Between the [quote] and [/quote] it will require to say the color needed, in the same format as block.
IE: Set a block with color green


```
#!php
[quote]
       [green] This is a test text [/green]
[/quote]

```

## Panel ##

Similar with quote tag, however this tag requires some settings, aligning and background color.

The default of the tags will be a grey full width section, this will apply only when no alignment or color is set. In case of missing tags, the default will be:

- Alignment : full width
- Color : grey

Tag options : 

* **Aligning** (a): left, right, full_width
* **Background color** (c) : blue, orange, green, pink, grey, light_blue, light_orange, light_green, light_pink, light_grey

The "" are not mandatory.

IE: Set an orange panel to the right


```
#!php

[panel a=right c=orange]
  This is a dummy text
[/panel]

```

# File Structure #

```
#!php
 shortcodes.php
 core/
      controller/
                panel.php
                quote.php
      library/
              style.php

```

* shortcodes.php => registers the sortcodes (panel, block)
* style.php => sets the inline style for all shortcodes, to be able to export into PDF
* controller/ => generates and sets the shortcodes.