<?php
/*
Plugin Name: ShortCodes
Description: Custom shortcodes
Version: v1.0
Author: Ovidiu S Bokar
Author URI: http://www.bovidiu.com
*/

include __DIR__.'/core/controller/quote.php';
include __DIR__.'/core/controller/panel.php';

function quote_method( $atts, $content = null ) {
    $quote = new Quote();
    return $quote->initGrabber($content);
}
add_shortcode( 'quote', 'quote_method' );

function panel_method( $atts, $content = null ) {
	$panel = new PanelBoard();
	return $panel->initPanelBoard($atts, $content);
}
add_shortcode( 'panel', 'panel_method' );
?>